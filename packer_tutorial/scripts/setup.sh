#!/bin/bash

set -x

# Add vagrant user
#echo 'vagrant' | sudo -S useradd vagrant -m -p vagrant
# whoami
# groups vagrant
# echo 'vagrant' | sudo -S usermod -a -G sudo vagrant
# echo 'vagrant' | sudo -S su vagrant

# user vagrant should not be asked to type in the password. Makes automation extremly difficult without this:
echo 'vagrant ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/vagrant

# Install necessary dependencies
#echo 'vagrant' | 
apt-get -y update
# echo 'vagrant'
# echo 'yes'

# Installing SSH key
# echo 'vagrant' | sudo -S 
mkdir -p /home/vagrant/.ssh
# echo 'vagrant' | sudo -S 
chmod 700  /home/vagrant/.ssh
# echo 'vagrant' | sudo -S 
wget https://raw.githubusercontent.com/hashicorp/vagrant/main/keys/vagrant.pub
mv vagrant.pub /home/vagrant/.ssh/authorized_keys
# cp ./vagrant-ssh.pub /home/vagrant/.ssh/authorized_keys
# echo 'vagrant' | sudo -S 
chmod 600 /home/vagrant/.ssh/authorized_keys
# echo 'vagrant' | sudo -S 
chown -R vagrant /home/vagrant/.ssh

# Install VirtualBox Guest Additions
# Noch nicht gut. Die Passworteingabe funktioniert nicht richtig so.
# Man sollte vorübergehend das sudoers-File anpassen, damit keine Passwort-Eingaben nötig sind
#echo 'vagrant' | 
apt-get -y install linux-headers-$(uname -r) build-essential dkms
# echo 'y'

wget http://download.virtualbox.org/virtualbox/7.0.10/VBoxGuestAdditions_7.0.10.iso
#echo 'vagrant' | 
mkdir -p /media/VBoxGuestAdditions
#echo 'vagrant' | 
mount -o loop,ro VBoxGuestAdditions_7.0.10.iso /media/VBoxGuestAdditions
#echo 'vagrant' | 
sh /media/VBoxGuestAdditions/VBoxLinuxAdditions.run
rm ./VBoxGuestAdditions_7.0.10.iso
#echo 'vagrant' | 
umount /media/VBoxGuestAdditions
#echo 'vagrant' | 
rmdir /media/VBoxGuestAdditions
# sudo mkdir -p /vagrant
