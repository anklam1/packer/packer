packer {
  required_plugins {
    virtualbox = {
      version = "~> 1"
      source  = "github.com/hashicorp/virtualbox"
    }
    vagrant = {
      version = ">= 1.1.1"
      source  = "github.com/hashicorp/vagrant"
    }
  }
}

source "virtualbox-iso" "ubuntu" {
  guest_os_type    = "Ubuntu_64"
  iso_url          = "http://releases.ubuntu.com/22.04/ubuntu-22.04.3-live-server-amd64.iso"
  iso_checksum     = "sha256:a4acfda10b18da50e2ec50ccaf860d7f20b389df8765611142305c0e911d16fd"
  ssh_username     = "vagrant"
  ssh_password     = "vagrant"
  shutdown_command = "echo 'vagrant' | sudo -S shutdown -P now"
  memory           = 16384
  cd_files         = ["./meta-data", "./user-data"]
  cd_label         = "cidata"
  ssh_timeout      = "20m"
  boot_wait        = "5s"
  boot_command = [

    "<enter>",
    # Grösstenteils Workaround um automatisch die Installation fortzusetzen ohne Eingabe tätigen zu müssen
    # "zes" weil es das schweizer Tastaturlayout erst später zu übernehmen scheint oder nicht in der automatischen Eingabe übernimmt
    "<wait50s>zes<enter>"

  ]
}

build {
  sources = ["sources.virtualbox-iso.ubuntu"]

  # Möglichkeit Files einzubinden, welche die VM dann als eigene gebrauchen kann
  # provisioner "file" {
  #   source      = "./vagrant-ssh.pub"
  #   destination = "./vagrant-ssh.pub"
  # }

  # Einbindung eines Shell-Skripts um weitere Cmd-Befehle auszuführen, nach dem Bootvorgang, wenn die VM gestartet ist
  provisioner "shell" {
    execute_command = "echo 'vagrant' | sudo -S sh -c '{{ .Path }}'"
    script          = "./scripts/setup.sh"
  }

  post-processor "vagrant" {
  }
}
